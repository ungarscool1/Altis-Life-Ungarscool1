/*
    Uniform Script
    Author: Eldayia
    Created for http://altisdev.com
*/

    #define __GETC__(var) (call var)
    
    if (playerSide != independent) then {
        if ((uniform player) == "U_Rangemaster")  then {
            player setObjectTextureGlobal [0, "textures\Polo_Gendarmerie.paa"]; //Tenue d'intendant - Cop
        };
    } else {
        if ((uniform player) == "U_Rangemaster")  then {
            player setObjectTextureGlobal [0, "textures\Polo_Pompiers.paa"]; //Tenue d'intendant - Medic
        };
    };
