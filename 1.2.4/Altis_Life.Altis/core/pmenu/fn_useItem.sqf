/*
	File: fn_useItem.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main function for item effects and functionality through the player menu.
*/
private["_item"];
disableSerialization;
if((lbCurSel 2005) == -1) exitWith {hint localize "STR_ISTR_SelectItemFirst";};
_item = lbData[2005,(lbCurSel 2005)];

switch (true) do
{
// DDE
case (_item == "mur"):
	{
		if(!isNull life_mur) exitWith {hint "Vous deployez deja cet objet"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[_item] spawn life_fnc_poseitem;
		};
	};
	
	case (_item == "sac"):
	{
		if(!isNull life_sac) exitWith {hint "Vous deployez deja cet objet"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[_item] spawn life_fnc_poseitem;
		};
	};
	
	case (_item == "cone"):
	{
		if(!isNull life_cone) exitWith {hint "Vous deployez deja cet objet"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[_item] spawn life_fnc_poseitem;
		};
	};
	
	case (_item == "barre"):
	{
		if(!isNull life_barre) exitWith {hint "Vous deployez deja cet objet"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[_item] spawn life_fnc_poseitem;
		};
	};
	
	case (_item == "lightr"):
	{
		if(!isNull life_lightr) exitWith {hint "Vous deployez deja cet objet"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[_item] spawn life_fnc_poseitem;
		};
	};

	case (_item == "lightg"):
	{
		if(!isNull life_lightg) exitWith {hint "Vous deployez deja cet objet"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[_item] spawn life_fnc_poseitem;
		};
	};
	
	case (_item == "lighty"):
	{
		if(!isNull life_lighty) exitWith {hint "Vous deployez deja cet objet"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[_item] spawn life_fnc_poseitem;
		};
	};
		
	case (_item == "flecheg"):
	{
		if(!isNull life_barre) exitWith {hint "Vous deployez deja cet objet"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[_item] spawn life_fnc_poseitem;
		};
	};
	
	case (_item == "fleched"):
	{
		if(!isNull life_barre) exitWith {hint "Vous deployez deja cet objet"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[_item] spawn life_fnc_poseitem;
		};
	};

// Drink
	case (_item =="bottledwhiskey"):
	{
		if(playerSide in [west,independent]) exitWith {hint localize "STR_MISC_WestIndNoNo";};
		if((player getVariable ["inDrink",FALSE])) exitWith {hint localize "STR_MISC_AlreadyDrinking";};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if(isNil "life_drink") then {life_drink = 0;};
			life_drink = life_drink + 0.06;
			if (life_drink < 0.07) exitWith {};
			[] spawn life_fnc_drinkwhiskey;
		};
	};
	
	case (_item =="bottledshine"):
	{
		if(playerSide in [west,independent]) exitWith {hint localize "STR_MISC_WestIndNoNo";};
		if((player getVariable ["inDrink",FALSE])) exitWith {hint localize "STR_MISC_AlreadyDrinking";};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if(isNil "life_drink") then {life_drink = 0;};
			life_drink = life_drink + 0.08;
			if (life_drink < 0.09) exitWith {};
			[] spawn life_fnc_drinkmoonshine;
		};
	};
	
	case (_item =="bottledbeer"):
	{
		
		if(playerSide in [west,independent]) exitWith {hint localize "STR_MISC_WestIndNoNo";};
		if((player getVariable ["inDrink",FALSE])) exitWith {hint localize "STR_MISC_AlreadyDrinking";};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if(isNil "life_drink") then {life_drink = 0;};
			life_drink = life_drink + 0.02;
			if (life_drink < 0.06) exitWith {};
			[] spawn life_fnc_drinkbeer;
		};
	};
// Do not modify

	case (_item == "couche"): {
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_toilet = 100;
			[true,"couchesale",1] call life_fnc_handleInv;
			hint "Tu t'est chier deçus... Achète du papier la prochaine fois!.";
		};
	};

// End to do not modify

	case (_item == "water" or _item == "coffee"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			player setFatigue 0;
		};
	};
	
	case (_item == "boltcutter"): {
		[cursorTarget] spawn life_fnc_boltcutter;
		closeDialog 0;
	};
	
	case (_item == "blastingcharge"): {
		player reveal fed_bank;
		(group player) reveal fed_bank;
		[cursorTarget] spawn life_fnc_blastingCharge;
	};
	
	case (_item == "defusekit"): {
		[cursorTarget] spawn life_fnc_defuseKit;
	};
	
	case (_item in ["storagesmall","storagebig"]): {
		[_item] call life_fnc_storageBox;
	};
	
	case (_item == "redgull"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			player setFatigue 0;
			[] spawn
			{
				life_redgull_effect = time;
				titleText[localize "STR_ISTR_RedGullEffect","PLAIN"];
				player enableFatigue false;
				waitUntil {!alive player OR ((time - life_redgull_effect) > (3 * 60))};
				player enableFatigue true;
			};
		};
	};
	
	case (_item == "spikeStrip"):
	{
		if(!isNull life_spikestrip) exitWith {hint localize "STR_ISTR_SpikesDeployment"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_spikeStrip;
		};
	};
	
	case (_item == "fuelF"):
	{
		if(vehicle player != player) exitWith {hint localize "STR_ISTR_RefuelInVehicle"};
		[] spawn life_fnc_jerryRefuel;
	};
	
	case (_item == "lockpick"):
	{
		[] spawn life_fnc_lockpick;
	};
	
	case (_item in ["apple","rabbit","salema","ornate","mackerel","tuna","mullet","catshark","turtle","turtlesoup","donuts","tbacon","peach"]):
	{
		[_item] call life_fnc_eatFood;
	};

	case (_item == "pickaxe"):
	{
		[] spawn life_fnc_pickAxeUse;
	};
	
	default
	{
		hint localize "STR_ISTR_NotUsable";
	};
};
	
[] call life_fnc_p_updateMenu;
[] call life_fnc_hudUpdate;