/*
	File: fn_virt_shops.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Config for virtual shops.
*/
private["_shop"];
_shop = _this select 0;

switch (_shop) do
{
	case "market": {["Marché d'Altis",["couche","water","rabbit","apple","redgull","tbacon","lockpick","pickaxe","fuelF","peach","boltcutter","storagesmall","storagebig"]]};
	case "rebel": {["Marché Rebel",["couche","water","rabbit","apple","redgull","tbacon","lockpick","pickaxe","fuelF","peach","boltcutter","blastingcharge"]]};
	case "gang": {["Marché de gang", ["couche","water","rabbit","apple","redgull","tbacon","lockpick","pickaxe","fuelF","peach","blastingcharge","boltcutter"]]};
	case "wongs": {["Wong's Food Cart",["turtlesoup","turtle"]]};
	case "coffee": {["Stratis Coffee Club",["coffee","donuts"]]};
	case "heroin": {["Drug Dealer",["cocainep","heroinp","marijuana"]]};
	case "oil": {["Oil Trader",["oilp","pickaxe","fuelF"]]};
	case "fishmarket": {["Poissonerie",["salema","ornate","mackerel","mullet","tuna","catshark"]]};
	case "glass": {["Altis Glass Dealer",["glass"]]};
	case "iron": {["Altis Industrial Trader",["iron_r","copper_r"]]};
	case "diamond": {["Diamond Dealer",["diamond","diamondc"]]};
	case "salt": {["Salt Dealer",["salt_r"]]};
	case "cop": {["Magasin de police",["couche","donuts","coffee","spikeStrip","water","rabbit","apple","redgull","fuelF","defusekit","mur","sac","cone","barre","lightr","lightg","lighty","flecheg","fleched"]]};
	case "cement": {["Cement Dealer",["cement"]]};
	case "gold": {["Racheteur d'Or",["goldbar"]]};
	case "bar": {["Le bar au coin de la rue",["bottledbeer","bottledwhiskey"]]};
	case "speakeasy": {["Le bar caché",["bottledwhiskey","bottledshine","bottledbeer","moonshine"]]};
	case "fabriqbar": {["Le bar caché",["cornmeal","bottles"]]};
	
};